/*
 * Copyright (C) 2017-2020 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    clipboard.c
 * @brief   Resource manager implementing a simple clipboard service
 *
 * The clipboard resource manager exposes a directory populated with "files",
 * each representing data available in a particular format (such as plain text,
 * HTML, JPEG image, etc.). Data is copied to the clipboard by opening a file
 * with the requested format as its name (potentially creating a new node in the
 * directory), and writing the data to the file. Data is pasted from the
 * clipboard by opening the same file and reading its contents. The list of
 * available formats can be determined by traversing the directory with
 * readdir() and checking for non-empty files.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <sys/neutrino.h>
#include <sys/iofunc.h>
#include <sys/dispatch.h>
#include <sys/procmgr.h>

typedef struct clipboard_entry  clipboard_entry_t;

/**
 * A node under the mount point that holds the data for a single format type
 * (e.g., text/html).
 */
struct clipboard_entry
{
    iofunc_attr_t       attr;
    clipboard_entry_t   *next;
    char                *name;
    size_t              name_len;
    char                *data;
};

static resmgr_connect_funcs_t   connect_funcs;
static resmgr_io_funcs_t        io_funcs;
static iofunc_attr_t            io_attr;
static clipboard_entry_t        *clipboard_entries;

/**
 * Read-only .qnxclipboard node.
 * Used to advertise the fact that this directory exposes a clipboard.
 */
static char const               qnxclipboard[] = ".qnxclipboard";
static clipboard_entry_t        qnxclipboard_entry = {
    .name = (char *)qnxclipboard,
    .name_len = sizeof(qnxclipboard)
};

/**
 * Handles an _IO_CONNECT message.
 * This message can be sent for the directory, as a result of an opendir() call,
 * or to a particular node, from an open(), stat() or unlink() calls.
 * The function associates an open control block (OCB) object with the attribute
 * structure for the matching node. If no node exists and O_CREAT is specified,
 * a new one is created and added to the linked list of nodes.
 * @param   ctp     Message context
 * @param   msg     Connect message
 * @param   dirattr Pointer to the attribute structure for the directory
 * @param   extra   Opaque pointer from the resource manager library
 * @return  EOK if successful, error code otherwise
 */
static int
open_clipboard(
    resmgr_context_t * const    ctp,
    io_open_t * const           msg,
    iofunc_attr_t * const       dirattr,
    void * const extra
)
{
    // An empty path means that the directory was opened (for a readdir() call).
    if (msg->connect.path[0] == '\0') {
        return iofunc_open_default(ctp, msg, dirattr, extra);
    }

    // Find an entry matching the requested one.
    clipboard_entry_t   *entry;
    for (entry = clipboard_entries; entry != NULL; entry = entry->next) {
        if (strncmp(msg->connect.path, entry->name, msg->connect.path_len)
            == 0) {
            // Found.
            return iofunc_open_default(ctp, msg, &entry->attr, extra);
        }
    }

    // Not found, check if the entry should be created.
    if ((msg->connect.ioflag & O_CREAT) == 0) {
        return ENOENT;
    }

    // Create a new entry.
    entry = malloc(sizeof(*entry));
    if (entry == NULL) {
        return ENOMEM;
    }

    entry->name = malloc(msg->connect.path_len);
    if (entry->name == NULL) {
        free(entry);
        return ENOMEM;
    }

    // Initialize the entry.
    strncpy(entry->name, msg->connect.path, msg->connect.path_len);
    entry->name_len = msg->connect.path_len;
    entry->data = NULL;

    iofunc_attr_init(&entry->attr, S_IFREG | 0600, NULL, NULL);

    // Put on the linked list.
    entry->next = clipboard_entries;
    clipboard_entries = entry;
    return iofunc_open_default(ctp, msg, &entry->attr, extra);
}

/**
 * Handles an _IO_CONNECT_UNLINK message.
 * Removes a node and cleans up its associated data.
 * @param   ctp     Message context
 * @param   msg     Unlink message
 * @param   dattr   Pointer to the attribute structure for the directory
 * @param   extra   Opaque pointer from the resource manager library
 * @return  EOK if successful, error code otherwise
 */
static int
delete_clipboard(
    resmgr_context_t * const    ctp,
    io_unlink_t * const         msg,
    iofunc_attr_t * const       dirattr,
    void * const                extra
)
{
    if (msg->connect.path[0] == '\0') {
        // Cannot unlink the directory itself.
        return EBUSY;
    }

    // Find an entry matching the requested one.
    clipboard_entry_t   *entry;
    clipboard_entry_t   **pprev = &clipboard_entries;
    for (entry = clipboard_entries; entry != NULL; entry = entry->next) {
        if (strncmp(msg->connect.path, entry->name, msg->connect.path_len)
            == 0) {
            // Found.
            // Delete the entry.
            int const   err = iofunc_unlink(ctp, msg, &entry->attr, dirattr,
                                            NULL);

            if (err != EOK) {
                return err;
            }

            *pprev = entry->next;
            free(entry);
            return EOK;
        }

        pprev = &entry->next;
    }

    return ENOENT;
}

// The size of the data in a dirent_extra_stat structure. Used to populate the
// d_datalen field in the dirent_extra structure.
#define EXTRA_STAT_LEN                                          \
    (sizeof(struct dirent_extra_stat) - sizeof(struct dirent_extra))

/**
 * Handles an _IO_READ message on the directory.
 * The reply is an array of dirent structures, one for each node under the
 * directory, beginning with the last offset read (typically 0).
 * @param   ctp     Message context
 * @param   msg     Read message
 * @param   ocb     Control block for the open file
 * @return  Value encoding the reply IOV
 */
static int
read_directory(
    resmgr_context_t * const    ctp,
    io_read_t * const           msg,
    iofunc_ocb_t * const        ocb
)
{
    size_t  max_reply_len = msg->i.nbytes;
    int     need_extra = 0;

    // Never go beyond the end of the resource manager buffer.
    if (max_reply_len > (ctp->msg_max_size - ctp->offset)) {
        max_reply_len = ctp->msg_max_size - ctp->offset;
    }

    if (msg->i.xtype & _IO_XFLAG_DIR_EXTRA_HINT) {
        need_extra = 1;
    }

    // Generate a reply populated with dirent records.
    struct dirent  *dir = (struct dirent *)msg;
    size_t          offset = 0;
    size_t          reply_len = 0;
    for (clipboard_entry_t *entry = clipboard_entries;
         entry != NULL;
         entry = entry->next) {
        // Skip entries below the current OCB offset.
        if (offset < ocb->offset) {
            offset++;
            continue;
        }

        // Make sure we have enough room for a dirent structure header before we
        // start populating it.
        if ((reply_len + sizeof(struct dirent)) > max_reply_len) {
            break;
        }

        // Initialize the record.
        dir->d_offset = offset;
        dir->d_namelen = entry->name_len - 1;

        // Determine the total size of the record.
        struct dirent_extra *extra = _DEXTRA_FIRST(dir);
        uintptr_t           rec_len = (uintptr_t)extra - (uintptr_t)dir;
        if (need_extra) {
            rec_len += sizeof(struct dirent_extra_stat);
        }

        // Now that we have calculated the total size of the structure, we can
        // determine if the reply buffer is big enough for it.
        if ((reply_len + rec_len) > max_reply_len) {
            break;
        }

        // Finish populating the record.
        strcpy(dir->d_name, entry->name);
        dir->d_reclen = rec_len;
        if (need_extra) {
            unsigned    format = _STAT_FORM_PREFERRED;
            unsigned    len;
            struct stat *stp = (struct stat *)(extra + 1);
            iofunc_stat_format(ctp, &entry->attr, &format, stp, &len);
            extra->d_datalen = EXTRA_STAT_LEN;
            extra->d_type = _DTYPE_LSTAT;
        }

        // Advance to the next dirent structure.
        dir = (struct dirent *)((char *)dir + rec_len);
        reply_len += rec_len;
        offset++;
    }

    // Reply to the caller.
    ctp->status = reply_len;
    ocb->offset = offset;
    return reply_len == 0 ? EOK : _RESMGR_PTR(ctp, msg, reply_len);
}

/**
 * Handles an _IO_READ message.
 * If the OCB refers to the directory, the function calls read_directory().
 * Otherwise, the data from the referenced node is copied into the reply buffer,
 * starting from the last offset (typically 0).
 * @param   ctp     Message context
 * @param   msg     Read message
 * @param   ocb     Control block for the open file
 * @return  Value encoding the reply IOV
 */
static int
read_clipboard(
    resmgr_context_t * const    ctp,
    io_read_t * const           msg,
    iofunc_ocb_t * const        ocb
)
{
    if (ocb->attr->mode & S_IFDIR) {
        return read_directory(ctp, msg, ocb);
    }

    clipboard_entry_t * const   entry = (clipboard_entry_t *)ocb->attr;
    if (ocb->offset >= entry->attr.nbytes) {
        ctp->status = 0;
        return EOK;
    }

    // Determine the length of the reply.
    size_t  reply_len = entry->attr.nbytes - ocb->offset;
    if (reply_len > (ctp->msg_max_size - ctp->offset)) {
        reply_len = ctp->msg_max_size - ctp->offset;
    };

    // Reply to the caller.
    memcpy((char *)msg, &entry->data[ocb->offset], reply_len);
    ctp->status = reply_len;
    ocb->offset += reply_len;
    return _RESMGR_PTR(ctp, msg, reply_len);
}

/**
 * Handles an _IO_WRITE message.
 * Overwrites the data associated with the node referred to by the OCB.
 * @param   ctp     Message context
 * @param   msg     Write message
 * @param   ocb     Control block for the open file
 * @return  EOK if successful, error code otherwise
 */
static int
write_clipboard(
    resmgr_context_t * const    ctp,
    io_write_t * const          msg,
    iofunc_ocb_t * const        ocb
)
{
    // Can't write a directory.
    if (ocb->attr->mode & S_IFDIR) {
        return ENOTSUP;
    }

    // Can only write from the beginning.
    if (ocb->offset != 0) {
        return ENOTSUP;
    }

    // Determine the number of bytes to write.
    // The resource manager frameword ensures that at least the message header
    // is available.
    // Note: this limits the copied data to the size that fits within the
    // resource manager buffer, which is roughly 2K (set in main()). We could
    // get fancier and also MsgRead() extra bytes to support larger amounts of
    // copied data.
    size_t  nbytes = ctp->size - sizeof(msg->i);

    // Resize the data buffer to hold the new data.
    clipboard_entry_t   *entry = (clipboard_entry_t *)ocb->attr;
    entry->data = realloc(entry->data, nbytes);
    if (entry->data == NULL) {
        return ENOMEM;
    }

    // Update the data.
    memcpy(entry->data, (char *)(&msg->i + 1), nbytes);
    entry->attr.nbytes = nbytes;

    // Reply to the caller.
    ocb->offset += nbytes;
    ctp->status = nbytes;
    return EOK;
}

/**
 * Main function.
 * Initializes the resource manager and then runs the message loop.
 */
int
main(int argc, char **argv)
{
    uid_t       uid = getuid();
    const char  *mount = "/dev/clipboard";

    // Parse command-line options.
    for (;;) {
        int opt = getopt(argc, argv, "m:u:");
        if (opt == -1) {
            break;
        } else if (opt == 'm') {
            mount = optarg;
        } else if (opt == 'u') {
            uid = strtol(optarg, NULL, 0);
        } else {
            fprintf(stderr, "Unknown option: '%c\n", opt);
            return 1;
        }
    }

    // Retain the ability to attach to the mount point before switching UID.
    if (procmgr_ability(0,
                        PROCMGR_ADN_NONROOT |
                        PROCMGR_AOP_ALLOW |
                        PROCMGR_AID_PATHSPACE,
                        PROCMGR_AID_EOL)
        != 0) {
        perror("procmgr_ability");
        return 1;
    }

    // Switch UID.
    if (setreuid(uid, uid) == -1) {
        perror("setreuid");
        return 1;
    }

    // Initialize the node list.
    iofunc_attr_init(&qnxclipboard_entry.attr, S_IFREG | 0400, NULL, NULL);
    clipboard_entries = &qnxclipboard_entry;

    // Set up callback functions.
    iofunc_func_init(_RESMGR_CONNECT_NFUNCS, &connect_funcs,
                     _RESMGR_IO_NFUNCS, &io_funcs);

    connect_funcs.open = open_clipboard;
    connect_funcs.unlink = delete_clipboard;
    io_funcs.read = read_clipboard;
    io_funcs.write = write_clipboard;

    // Initialize directory attributes.
    iofunc_attr_init(&io_attr, S_IFDIR | 0700, 0, 0);
    io_attr.nbytes = 4096;

    // Initialize resource manager attributes.
    resmgr_attr_t       rattr;
    memset(&rattr, 0, sizeof(rattr));
    rattr.nparts_max = 1;
    rattr.msg_max_size = 2048;

    // Create the main dispatch structure.
    dispatch_t  *dispatch = dispatch_create();
    if (dispatch == NULL) {
        perror("dispatch_create");
        return 1;
    }

    // Attach to the mount point.
    int id = resmgr_attach(dispatch, &rattr, mount, _FTYPE_ANY, _RESMGR_FLAG_DIR,
                           &connect_funcs, &io_funcs, &io_attr);
    if (id < 0) {
        perror("resmgr_attach");
        return 1;
    }

    // Create the message context.
    dispatch_context_t  *ctx = dispatch_context_alloc(dispatch);
    if (ctx == NULL) {
        perror("dispatch_context_alloc");
        return 1;
    }

    // Message loop.
    for (;;) {
        ctx = dispatch_block(ctx);
        if (ctx == NULL) {
            perror("dispatch_block");
            return 1;
        }

        dispatch_handler(ctx);
    }

    return 0;
}
