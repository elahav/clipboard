ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

define PINFO
PINFO DESCRIPTION = Simple clipboard resource manager
endef
INSTALLDIR=
NAME=clipboard
USEFILE=

#EXTRA_INCVPATH=
#EXTRA_LIBVPATH=
#LIBS=
#CCOPTS=-g -O0

include $(MKFILES_ROOT)/qtargets.mk
