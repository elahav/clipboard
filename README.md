# Clipboard

Simple clipboard resource manager for QNX systems.

The clipboard resource manager exposes a directory populated with "files",
each representing data available in a particular format (such as plain text,
HTML, JPEG image, etc.). Data is copied to the clipboard by opening a file
with the requested format as its name (potentially creating a new node in the
directory), and writing the data to the file. Data is pasted from the
clipboard by opening the same file and reading its contents. The list of
available formats can be determined by traversing the directory with
readdir() and checking for non-empty files.
